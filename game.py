from random import randint

print("I have a secret 4-digit number.\nCan you guess it?\nYou have 20 tries.")

def convert_num_to_list(num):
    list = []
    for char in str(num):
        element = int(char)
        list.append(element)
    return list

secret_num_as_list = convert_num_to_list(randint(1000, 9999))

num_of_guesses = 1
while num_of_guesses < 21:
    bulls = 0
    cows = 0
    guess = input("Type in guess #" + str(num_of_guesses) + ": ")

    while len(guess) != 4:
        print("Only 4-digit numbers, please.")
        guess = input("Type in guess #" + str(num_of_guesses) + ": ")

    for a, b in zip(secret_num_as_list, convert_num_to_list(guess)):
        if a == b:
            bulls = bulls + 1
            if bulls == 4:
                end_reply = input("You guessed my secret!! Do you want to play again? ")
                if end_reply != "yes":
                    exit()
                else:
                    num_of_guesses = 0
                    secret_num_as_list = convert_num_to_list(randint(1000, 9999))
                    break
        else:
            if a in convert_num_to_list(guess):
                    cows = cows + 1

    if (bulls < 4) & (num_of_guesses < 20):
        print("You have " + str(bulls) + " bulls")
        print("You have " + str(cows) + " cows")

    while num_of_guesses == 20:
        lose_reply = input("\nThe secret was " + str(secret_num_as_list) + "\nDo you want to play again? ")
        if lose_reply != "yes":
            exit()
        else:
            num_of_guesses = 0
            secret_num_as_list = convert_num_to_list(randint(1000, 9999))
            break

    num_of_guesses = num_of_guesses + 1

